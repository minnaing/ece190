
compile:
	javac DeviceConnect.java TcpClient.java TestDevices.java

run:
	java TestDevices DeviceConnect TcpClient

runserver:
	python DemoServer/Server.py

clean:
	rm -rf *.class
