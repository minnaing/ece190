/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package connectandrino;

/**
 *
 * @author hannaing
 */
public class Config {
    public static int TURN = 3; // 3-4
    public static int SPEED = 5; // 1-60

    public static String HOST = "192.168.43.229" ; //77.105.99.104";
    public static int PORT = 80;

    public static int MIN_TURN = 1;
    public static int MAX_TURN = 6;

    public static int MIN_SPEED = 1;
    public static int MAX_SPEED = 6;
}
