/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package connectandrino;

/**
 *
 * @author hannaing
 */
import java.io.IOException;


public class DeviceConnect {
    protected String address;
    protected int port;
    protected String name;

    public DeviceConnect(String name, String address, int port){
        this.address = address;
        this.name = name;
        this.port = port;
    }

    // For setting updating
    public boolean update(String opt, String val) throws IOException{
        String cmd = opt + ',' + val;
        TcpClient.send(cmd, this.address, this.port);

        // We will identify whether setting us updated or not with return
        // But so far haven't implemented.
        return true;
    }

    public boolean open(int speed) throws IOException{
        String cmd = "$Open" + ' ' + speed;
        TcpClient.send(cmd, this.address, this.port);
        return true;
    }

    public boolean close(int speed) throws IOException{
        String cmd = "$Close" + ' ' + speed;
        TcpClient.send(cmd, this.address, this.port);
        return true;
    }

    public boolean setTime() throws IOException{
       String cmd = "time" + ' ' + System.currentTimeMillis();
       TcpClient.send(cmd, this.address, this.port);
       return true;
    }

    public boolean setTime(long unixTime) throws IOException{
       String cmd = "time" + ' ' + unixTime;
       TcpClient.send(cmd, this.address, this.port);
       return true;
    }
	// More action
	public boolean half() throws IOException{
        String cmd = "$Half";
        TcpClient.send(cmd, this.address, this.port);
        return true;
    }
	public boolean quarter() throws IOException{
        String cmd = "$Quarter";
        TcpClient.send(cmd, this.address, this.port);
        return true;
    }
	public boolean threeQuarter() throws IOException{
        String cmd = "$3Quarter";
        TcpClient.send(cmd, this.address, this.port);
        return true;
    }
	public boolean setTurn(int turn) throws IOException{
        String cmd = "$Max Turns " + turn;
        TcpClient.send(cmd, this.address, this.port);
        return true;
    }
}
