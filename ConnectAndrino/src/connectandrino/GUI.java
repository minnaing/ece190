/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package connectandrino;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;

/**
 *
 * @author hannaing
 */
public class GUI {
    private final JFrame mainFrame;
    private final JPanel hostPanel;
    private final JPanel controlPanel;
    private final JPanel resultPanel;
    private final JPanel settingPanel;
    private JLabel ipLabel;
    private JLabel portLabel;
    private JTextField ipText;
    private JTextField portText;
    private JButton setTime;
    private final JLabel status;
    private JButton openBTN;
    private JButton closeBTN;

    private JLabel speedLabel;
    private JLabel turnsLabel;
    private JTextField speedText;
    private JTextField turnsText;


	private JButton turnBTN, quarterBTN, threeQuarterBTN, halfBTN;

	private JComboBox speedCombo;
	private JComboBox turnCombo;

    private DeviceConnect dc;

    private final int WIDTH = 400;
    private final int HEIGHT = 300;

	private String[] SPEED_RANGE = { "1", "5", "10", "15", "20", "25", "30"};
	private String[] TURN_RANGE = { "1", "2", "3", "4", "5", "6"};

    public static void main(String[] args){
        GUI  gui = new GUI();
        gui.run();
   }

    public GUI(){

        mainFrame = new JFrame("Connect Andrino");
        mainFrame.setSize(WIDTH, 400);
        mainFrame.setLayout(new GridLayout(4, 1));// new BoxLayout());
        mainFrame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent windowEvent){
                System.exit(0);
            }

        });

        status = new JLabel("", JLabel.CENTER);
        status.setSize(WIDTH, 100);

        hostPanel = new JPanel(new FlowLayout());
        controlPanel = new JPanel(new FlowLayout());
        resultPanel = new JPanel(new FlowLayout());
        settingPanel = new JPanel(new FlowLayout());

        mainFrame.add(hostPanel);
        mainFrame.add(controlPanel);
        mainFrame.add(resultPanel);
        mainFrame.add(settingPanel);



        JLabel cmdLabel = new JLabel("Command : ", SwingConstants.CENTER);
        cmdLabel.setSize(WIDTH, 60);
        JLabel statusLabel = new JLabel("Status : ", JLabel.CENTER);
        statusLabel.setSize(WIDTH, 60);

        //mainFrame.add(cmdLabel);
        //mainFrame.add(statusLabel);
        resultPanel.add(status);
        resultPanel.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createEtchedBorder(), "Status"));
        mainFrame.setLocationRelativeTo(null);
        //mainFrame.pack();
        mainFrame.setVisible(true);

    }

    public String getNow(){
        DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
        Date date = new Date();
        return dateFormat.format(date);
    }

    public void run(){
        //buildHostPanel();
        ipLabel = new JLabel("IP Address    ", JLabel.CENTER);
        ipText = new JTextField(20);
        ipText.setText(Config.HOST);

        portLabel = new JLabel("Port Number ", JLabel.CENTER);
        portText = new JTextField(20);
        portText.setText("" + Config.PORT);

        hostPanel.add(ipLabel);
        hostPanel.add(ipText);
        hostPanel.add(portLabel);
        hostPanel.add(portText);
        hostPanel.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createEtchedBorder(), "Host Info"));

        // === END ===
		/*
        setTime = new JButton("Set Time");
        setTime.addActionListener((ActionEvent e) -> {
            String data = ipText.getText() + ":" + portText.getText() +
                    " >>> " + getNow();

            dc = new DeviceConnect("Device Anonymous", ipText.getText(), Integer.parseInt(portText.getText()));
            try {
                dc.setTime();
            } catch (IOException ex) {
                Logger.getLogger(GUI.class.getName()).log(Level.SEVERE, null, ex);
            }

            status.setText(data);
        });
		*/

		turnBTN = new JButton("Set Max Turn");
        turnBTN.addActionListener((ActionEvent e) -> {
			int turn = Integer.parseInt((String)turnCombo.getSelectedItem());
            String data = ipText.getText() + ":" + portText.getText() +
                    " >>> " + " setting max turn " + turn + "...";

            dc = new DeviceConnect("Device Anonymous", ipText.getText(), Integer.parseInt(portText.getText()));
            try {
                dc.setTurn(turn);
            } catch (IOException ex) {
                Logger.getLogger(GUI.class.getName()).log(Level.SEVERE, null, ex);
            }

            status.setText(data);
        });


        openBTN = new JButton("Open");
        openBTN.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                String data = ipText.getText() + ":" + portText.getText() +
                        " >>> " + " opening ...";

                dc = new DeviceConnect("Device Anonymous", ipText.getText(), Integer.parseInt(portText.getText()));
                try {
                    //dc.open(Integer.parseInt(speedText.getText()));
					dc.open(Integer.parseInt((String)speedCombo.getSelectedItem()));
                } catch (IOException ex) {
                    Logger.getLogger(GUI.class.getName()).log(Level.SEVERE, null, ex);
                }

                status.setText(data);
            }
        });

        closeBTN = new JButton("Close");
        closeBTN.addActionListener((ActionEvent e) -> {
            String data = ipText.getText() + ":" + portText.getText() +
                    " >>> " + " closing ...";

            dc = new DeviceConnect("Device Anonymous", ipText.getText(), Integer.parseInt(portText.getText()));
            try {
				/*
                int speed = Integer.parseInt(speedText.getText());
                if ( Helper.inRange(speed, Config.MIN_SPEED, Config.MAX_SPEED) ){
                    System.out.println("Speed ");
                }*/
                //dc.close(speed);
				dc.close(Integer.parseInt((String)speedCombo.getSelectedItem()));
            } catch (IOException ex) {
                Logger.getLogger(GUI.class.getName()).log(Level.SEVERE, null, ex);
            }

            status.setText(data);
        });

		// ====== [ More Function ] ======
		halfBTN = new JButton("Half");
        halfBTN.addActionListener((ActionEvent e) -> {
            String data = ipText.getText() + ":" + portText.getText() +
                    " >>> " + " Half ...";

            dc = new DeviceConnect("Device Anonymous", ipText.getText(), Integer.parseInt(portText.getText()));
            try {
				dc.half();
            } catch (IOException ex) {
                Logger.getLogger(GUI.class.getName()).log(Level.SEVERE, null, ex);
            }

            status.setText(data);
        });
		quarterBTN = new JButton("Quarter");
        quarterBTN.addActionListener((ActionEvent e) -> {
            String data = ipText.getText() + ":" + portText.getText() +
                    " >>> " + " Quarter ...";

            dc = new DeviceConnect("Device Anonymous", ipText.getText(), Integer.parseInt(portText.getText()));
            try {
				dc.quarter();
            } catch (IOException ex) {
                Logger.getLogger(GUI.class.getName()).log(Level.SEVERE, null, ex);
            }

            status.setText(data);
        });
		threeQuarterBTN = new JButton(" Three Quarter");
        threeQuarterBTN.addActionListener((ActionEvent e) -> {
            String data = ipText.getText() + ":" + portText.getText() +
                    " >>> " + " Three Quarter ...";

            dc = new DeviceConnect("Device Anonymous", ipText.getText(), Integer.parseInt(portText.getText()));
            try {
				dc.threeQuarter();
            } catch (IOException ex) {
                Logger.getLogger(GUI.class.getName()).log(Level.SEVERE, null, ex);
            }

            status.setText(data);
        });
		// ====== [ More Function ] ======



        JLabel cmdLabel = new JLabel("Command : ");
        cmdLabel.setSize(WIDTH, 60);
        JLabel statusLabel = new JLabel("Status : ");
        statusLabel.setSize(WIDTH, 60);


        controlPanel.setPreferredSize(new Dimension(WIDTH, 100));
        controlPanel.add(turnBTN);
        controlPanel.add(openBTN);
        controlPanel.add(closeBTN);
		controlPanel.add(quarterBTN);
		controlPanel.add(halfBTN);
        controlPanel.add(threeQuarterBTN);
        controlPanel.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createEtchedBorder(), "Action"));



        settingPanel.setPreferredSize(new Dimension(WIDTH, 100));
        speedLabel = new JLabel("Speed : ");
        //speedText = new JTextField(3);
		//speedText.setText("" + Config.SPEED);
		speedCombo = new JComboBox<>(SPEED_RANGE);
		speedCombo.setSelectedItem("" + Config.SPEED);

        turnsLabel = new JLabel("Turns : ");
        //turnsText = new JTextField(3);
        //turnsText.setText("" + Config.TURN);
		turnCombo = new JComboBox<>(TURN_RANGE);
		turnCombo.setSelectedItem("" + Config.TURN);




        settingPanel.add(speedLabel);
        //settingPanel.add(speedText);
		settingPanel.add(speedCombo);

		settingPanel.add(turnsLabel);
        //settingPanel.add(turnsText);
		settingPanel.add(turnCombo);

		settingPanel.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createEtchedBorder(), "Setting"));

        mainFrame.setVisible(true);
    }

    public void buildHostPanel(){
        ipLabel = new JLabel("IP Address    ", JLabel.CENTER);
        ipText = new JTextField(20);

        portLabel = new JLabel("Port Number ", JLabel.CENTER);
        portText = new JTextField(20);

        hostPanel.add(ipLabel);
        hostPanel.add(ipText);
        hostPanel.add(portLabel);
        hostPanel.add(portText);
        hostPanel.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createEtchedBorder(), "Host Info"));

        mainFrame.setVisible(true);
    }
}
