/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package connectandrino;

/**
 *
 * @author hannaing
 */
import java.io.IOException;



public class TestDevice {
    public static void main(String[] args) throws IOException{
        DeviceConnect d1 = new DeviceConnect("Device 1 on Bedroom", "localhost", 9000);
        DeviceConnect d2 = new DeviceConnect("Device 2 on Kitchen", "localhost", 9000);
        DeviceConnect d3 = new DeviceConnect("Device 3 on Livingroom", "localhost", 9000);
        DeviceConnect[] allDevices = {d1, d2, d3};

        d1.update("brightness", "90");
        d2.update("brightness", "100");
        d3.update("brightness", "105");

        d1.open(30);
        d1.open(60 * 60* 3); // next 3 hr

        for (DeviceConnect d: allDevices){
            d.close(60 * 60 * 24); // next day
        }
    }
}
