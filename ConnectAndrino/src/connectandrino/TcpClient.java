/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package connectandrino;

/**
 *
 * @author hannaing
 */

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Scanner;

public class TcpClient {
    public static void main(String[] args) throws IOException{
        Scanner reader = new Scanner(System.in);
        String address = "localhost";
        int port = 9123;
        while (true){
            System.out.print(">>> ");
            String s = reader.next();
            if ( s.equalsIgnoreCase("exit")){
                break;
            }
            send(s, address, port);
        }

    }

    public static String send(String msg, String address, int port) throws IOException{

        int TIMEOUT = 1000;
        //Socket client = new Socket(address, port);
        Socket client = new Socket();
        client.connect(new InetSocketAddress(address, port), TIMEOUT);
        OutputStream outBuffer = client.getOutputStream();
        DataOutputStream out = new DataOutputStream(outBuffer);
        out.writeUTF(msg);
		System.out.println(">>> Send :: " + msg);

        // Recieve
		String rtn = " ";
        client.setSoTimeout(TIMEOUT);
        try {
            InputStream inBuffer = client.getInputStream();
            DataInputStream in = new DataInputStream(inBuffer);
			rtn = ">>> Recieve reply :: " + in.readUTF();
            System.out.println(rtn);
			return rtn;
        }
        // Exception thrown when network timeout occurs
        catch (InterruptedIOException iioe){
			rtn = "Remote host timed out during read operation";
            System.err.println (rtn);
			return rtn;
        }
        // Exception thrown when general network I/O error occurs
        catch (IOException ioe){
			rtn = "Network I/O error - " + ioe;
            System.err.println (rtn);
			return rtn;
        }
		//return " ";
    }

    public static void recv(){
        // Do we need it ?
    }
}
